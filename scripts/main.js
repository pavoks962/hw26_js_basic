"use strict"

const images = [
    'https://picsum.photos/300/200?v1',
    'https://picsum.photos/300/200?v2',
    'https://picsum.photos/300/200?v3',
    'https://picsum.photos/300/200?v4',
    'https://picsum.photos/300/200?v5',
    'https://picsum.photos/300/200?v6',
  ]
  

const imgWrapper = document.querySelector('.img-wrapper');
const btnPrev = document.querySelector('.js-prev');
const btnNext = document.querySelector('.js-next');

const imagesHTML = images.map(el => `<img src="${el}">`);
imgWrapper.innerHTML = imagesHTML.slice(0, 1).join('');

let slide = 0;

btnNext.addEventListener('click', () => {
    slide++;
    if (slide > (images.length-1)) {
        slide = 0;
    }
    imgWrapper.innerHTML = imagesHTML.slice(`${slide}`, `${slide+1}`).join('');

})

btnPrev.addEventListener('click', () => {
    slide--;
    if (slide <= 0) {
        slide = images.length;
    }
    imgWrapper.innerHTML = imagesHTML.slice(`${slide-1}`, `${slide}`).join('');

})



